var content;
var produtos;


onload = function(e) {
	content = document.getElementById('content');
	showAds();
}

function execute(method, url) {
	var xmlhttp = new XMLHttpRequest();
	url += '?';
	xmlhttp.open(method, url, true);
	
	xmlhttp.onload = function (e) {
		  if (xmlhttp.readyState === 4) {
		    if (xmlhttp.status === 200) {
		    		console.log(xmlhttp.responseText);
		    		produtos = JSON.parse(xmlhttp.responseText);
		    		var html = new EJS({url: 'produtos.ejs'}).render(produtos);
					content.innerHTML = html;
		    } else {
		      console.error(xmlhttp.statusText);
		    }
		  }
		};
	
	xmlhttp.send(null);
}

function showAds() {
	execute('GET', '/ads');
}