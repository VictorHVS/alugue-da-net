var user;
var password;

$(document).ready(
		function() {
			$("#FormCadastro").submit(
					function(e) {
						e.preventDefault();
						var name = $("#FormName").val();
						var email = $("#FormEmail").val();
						var password = $("#FormPassword").val();
						var fone = $("#FormFone").val();

						var StrName = '{"name":"' + name + '"';
						var StrEmail = '"email":"' + email + '"';
						var StrPassword = '"password":"' + password + '"';
						var StrFone = '"fone":"' + fone + '"}';

						var userCad = StrName + ',' + StrEmail + ','
								+ StrPassword + ',' + StrFone;
						cadastrarUser(userCad);
					});
		});

$(document).ready(function() {
	$("#login").submit(function(e) {
		e.preventDefault();
		var username = $("#username").val();
		var password = $("#password").val();

		var strUsername = '"name":"' + username + '"';
		var strPassword = '"password":"' + password + '"';

		var str = '{' + strUsername + ',' + strPassword + '}';
		executeLogin(str);
	});

});

$(document).ready(function() {
	$("#FormCadastroAnuncio").submit(function(e) {
		var title = $("#FormTitle").val();
		var subTitle = $("#FormSubTitle").val();
		var price = $("#FormPrice").val();

		var StrTitle = '{"title":"' + title + '"';
		var StrSubTitle = '"subTitle":"' + subTitle + '"';
		var StrPrice = '"price":"' + price + '"}';

		var AdCad = StrTitle + ',' + StrSubTitle + ',' + StrPrice;

		cadastrarAd(AdCad);
	});
});

function execute_login(method, url, data) {
	var xmlhttp = new XMLHttpRequest();
	url += '?' + data;
	xmlhttp.open(method, url, true);

	xmlhttp.onload = function(e) {
		if (xmlhttp.readyState === 4) {
			if (xmlhttp.status === 200) {
				user = xmlhttp.responseText;
				var obj = JSON.parse(user);
				$("#rightTopBar").html("Bem vindo: " + obj.name);
			} else {
				console.error(xmlhttp.statusText);
			}
		}
	};

	xmlhttp.send(null);
}

function execute(method, url, data) {
	var xmlhttp = new XMLHttpRequest();
	url += '?' + data;
	xmlhttp.open(method, url, true);

	xmlhttp.onload = function(e) {
		if (xmlhttp.readyState === 4) {
			if (xmlhttp.status === 200) {
				alert("Cadastrado com sucesso");
				console.log(xmlhttp.responseText);
				// est = JSON.parse(xmlhttp.responseText);
				// var html = new EJS({url:
				// 'estacionamento.ejs?a=0'}).render(est);
				// $('#content').html(html);
			} else {
				console.error(xmlhttp.statusText);
			}
		}
	};

	xmlhttp.send(null);
}

function executeLogin(user) {
	execute_login('GET', '/user', 'user=' + user);
}

function login(user) {
	execute('GET', '/user', 'user=' + user);
}

function cadastrarUser(user) {
	execute('PUT', '/user', 'user=' + user);
}

function cadastrarAd(ad) {
	execute('PUT', '/ad', 'advertisement=' + ad);
}
