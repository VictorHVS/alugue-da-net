package br.com.bracode.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.bracode.Advertisement;
import br.com.bracode.tools.JDOUtils;

public class AdsServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		JDOUtils jdo 				= new JDOUtils();
		Gson	 g					= new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		List<Advertisement> ads		= jdo.findAll(Advertisement.class);
		
		resp.getWriter().print(g.toJson(ads));
	}
	
}
