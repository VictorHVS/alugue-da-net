package br.com.bracode.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bracode.Advertisement;
import br.com.bracode.User;
import br.com.bracode.tools.JDOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AdServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		JDOUtils jdo = new JDOUtils();
		
		Advertisement receivedAd = extractAd(req);
		Advertisement dbAd = jdo.findFirstByAttribute(Advertisement.class,"title", receivedAd.getTitle());
		if (dbAd != null) {
			Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
				resp.getWriter().println(g.toJson(dbAd));
			} else {
				resp.getWriter().println("error");
			}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JDOUtils jdo = new JDOUtils();
		Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		User user = UserServlet.checkLogin(jdo, UserServlet.extractUser(req));
		//User user = UserServlet.extractUser(req);
		Advertisement receivedAd = extractAd(req);

		if(user != null){
			user.getAdvertisements().add(receivedAd);
			jdo.beginTransaction();
			jdo.save(user);
			jdo.commit();
			resp.getWriter().println(g.toJson(user));
		} else {
			resp.getWriter().print("error");
		}
		
	}
	
	private static Advertisement extractAd(HttpServletRequest req) {
		Gson g = new Gson();
		String data = req.getParameter("advertisement");
	
		return g.fromJson(data, Advertisement.class);
	}

}
