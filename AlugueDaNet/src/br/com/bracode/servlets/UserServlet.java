package br.com.bracode.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bracode.User;
import br.com.bracode.tools.JDOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		JDOUtils jdo = new JDOUtils();

		User userReceived = extractUser(req);
		User dbUser = checkLogin(jdo, userReceived);
		
		if (dbUser != null) {
			Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			resp.getWriter().println(g.toJson(dbUser));
		} else {
			resp.getWriter().println("error");
		}
	}
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JDOUtils jdo = new JDOUtils();
		Gson g = new Gson();
		
		User user = extractUser(req);
		
		jdo.save(user);
		
		resp.getWriter().println(g.toJson(user));
	}

	public static User extractUser(HttpServletRequest req) {
		return new Gson().fromJson(req.getParameter("user"), User.class);
	}
	
	public static User checkLogin(JDOUtils jdo, User receivedUser) {
		User dbUser = jdo.findFirstByAttribute(User.class, "email", receivedUser.getEmail());
		if(dbUser != null && dbUser.login(receivedUser)){
			try {
				jdo.beginTransaction();
				jdo.save(dbUser);
				jdo.commit();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return dbUser;
		}
		return null;
	}
}
