package br.com.bracode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import com.google.appengine.api.datastore.Key;
import com.google.gson.annotations.Expose;

@PersistenceCapable(detachable = "true")
public class User {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	@Persistent
	@Expose
	private String name;
	@Expose
	private String avatar;
	@Expose @Unique
	private String email;
	@Expose
	private String password;
	@Expose
	private String fone;
	@Expose
	private String token;

	@Expose
	@Persistent(mappedBy = "user")
	private List<Advertisement> advertisements = new ArrayList<>();

	public User() {
	}
	
	public User(String name, String avatar, String email, String password,
			String fone, String token, List<Advertisement> advertisements) {
		super();
		this.name = name;
		this.avatar = avatar;
		this.email = email;
		this.password = password;
		this.fone = fone;
		this.token = token;
		this.advertisements = advertisements;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public List<Advertisement> getAdvertisements() {
		return advertisements;
	}

	public void setAdvertisements(List<Advertisement> advertisements) {
		this.advertisements = advertisements;
	}

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean login(User receivedUser) {
		boolean validUser = (this.email.equals(receivedUser.getEmail()));
		boolean validPassword = (this.password.equals(receivedUser
				.getPassword()));
		boolean validToken = false;
		if (receivedUser.getToken() != null && this.token != null) {
			validToken = (this.token.equals(receivedUser.getToken()));
		}
		if (validUser && (validPassword || validToken)) {
			setToken("" + new Random(System.nanoTime()).nextLong());
			return true;
		}

		return false;
	}

}
